FROM ruby:2.5.3

# Expose our server port.
EXPOSE 3000

ENV RAILS_ENV='production'
ENV RAKE_ENV='production'

RUN mkdir /pool-manager
WORKDIR /pool-manager

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs yarn

RUN gem install bundler
COPY Gemfile /pool-manager/Gemfile
COPY Gemfile.lock /pool-manager/Gemfile.lock

RUN bundle install
RUN yarn install --pure-lockfile

COPY . /pool-manager

RUN rails assets:precompile
