# Pool Manager

[![pipeline status](https://gitlab.com/einfachiota/pool-manager/badges/master/pipeline.svg)](https://gitlab.com/einfachiota/pool-manager/commits/master)

The pool manager is a helper to manage your iri nodes in a pool.

- Register your node to a pool.
- Manage multiple iris with one account.
- Donation distribution with Donation Service.
- Seasons (soon)
