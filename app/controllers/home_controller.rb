class HomeController < ApplicationController
  def index
    uri = URI("https://nutzdoch.einfachiota.de/nodes")
    response =  Net::HTTP.get(uri)# => String
    @nodes = JSON.parse(response)

    @nodes.each do |node|
      points = 0

      factor = 1
      node["commands"].each do |entry|
        pp entry
        case entry["command"]
          when 'attachToTangle'
            factor = 50
          when 'broadcastTransactions'
            factor = 5
          when 'checkConsistency'
            factor = 5
          when 'findTransactions'
            factor = 5
          when 'getBalances'
            factor = 3
          when 'getInclusionStates'
            factor = 3
          when 'getNodeInfo'
            factor = 1
          when 'getTransactionsToApprove'
            factor = 3
          when 'getTrytes'
            factor = 3
          when 'storeTransactions'
            factor = 20
          when 'wereAddressesSpentFrom'
            factor = 5
          else
            factor = 1
        end
        pp "factor"
        pp factor
        points = points + entry["count"] * factor - entry["millis"] / 1000
      end
      node["points"] = points
    end


  end

  def terms
  end

  def privacy
  end

  def donations
    uri = URI("#{Rails.configuration.dontation_manager_url}/donations.json")
    response =  Net::HTTP.get(uri)# => String
    @donations = JSON.parse(response)
  end
end
