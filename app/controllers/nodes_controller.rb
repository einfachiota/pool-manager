class NodesController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_node, only: [:show, :edit, :update, :destroy]

  # GET /nodes
  # GET /nodes.json
  def index
    @nodes = Node.all
  end

  # GET /nodes/1
  # GET /nodes/1.json
  def show
  end

  # GET /nodes/new
  def new
    @node = Node.new
    @register_new_node = params[:register_new_node] || "true"
  end

  # GET /nodes/1/edit
  def edit
  end

  # POST /nodes
  # POST /nodes.json
  def create
    @node = Node.new(node_params)

    if params[:node][:key]

      respond_to do |format|
        unless @node.validate! params[:node][:key]
          format.html { render :show }
          format.json { render json: @node.errors, status: :unprocessable_entity }
        end
      end
    end
    ## prüfe ob node key in den params ist und validate it!

    @node.user = current_user
    respond_to do |format|
      if @node.save
        format.html { redirect_to @node, notice: 'Node was successfully created.' }
        format.json { render :show, status: :created, location: @node }
      else
        format.html { render :new }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nodes/1
  # PATCH/PUT /nodes/1.json
  def update

    if params[:commit] == "Add key"

      respond_to do |format|
        if @node.validate! params[:node][:key]
          format.html { redirect_to @node, notice: 'Node was successfully updated.' }
          format.json { render :show, status: :ok, location: @node }
        else
          format.html { render :show }
          format.json { render json: @node.errors, status: :unprocessable_entity }
        end
      end
      # validate key
    else

      respond_to do |format|
        if @node.update(node_params)
          format.html { redirect_to @node, notice: 'Node was successfully updated.' }
          format.json { render :show, status: :ok, location: @node }
        else
          format.html { render :edit }
          format.json { render json: @node.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /nodes/1
  # DELETE /nodes/1.json
  def destroy
    @node.destroy
    respond_to do |format|
      format.html { redirect_to nodes_url, notice: 'Node was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def payout_data
    nodes = Node.where(status: :connected)
    uri = URI(Rails.configuration.distribution_service_url)
    response =  Net::HTTP.get(uri)# => String
    data = JSON.parse(response)

    pp "nodes.count"
    pp nodes.count
    pp "data.count"
    pp data.count
    @payout_data = []

    data.each do |entry|
      pp "entry.name"
      pp entry["name"]
      node = nodes.find_by_name(entry["name"])
      if node
        pp "found!"
        payout_object = {
          name: node.name,
          address: node.donation_address,
          points: entry["points"],
          share: entry["share"]
        }
        @payout_data.push(payout_object)
      end
    end
    @payout_data
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_node
      @node = Node.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def node_params
      params.require(:node).permit(:url, :name, :pow, :key, :donation_address)
    end
end
