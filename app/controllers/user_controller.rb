class UserController < ApplicationController
  before_action :authenticate_user!

  def nodes
    @nodes = current_user.nodes
  end

end
