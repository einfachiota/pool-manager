class Node < ApplicationRecord

  enum status: [:waiting, :connected]


  after_create :set_status_waiting


  require 'iota'

  validates_length_of :name, :minimum => 2, :maximum => 100
  validates_presence_of :url
  validate :valid_iota_address

  belongs_to :user

  def validate! _key

    require 'digest'
    require 'json'

    public_id = Digest::SHA2.hexdigest _key

    uri = URI('https://nutzdoch.einfachiota.de/nodes/' + URI::encode(name))
    response = Net::HTTP.get(uri) # => String
    data = JSON.parse(response)
    if public_id.starts_with? data["key"]
      self.connected!
      return true
    else
      errors.add(:key, :blank, message: "wrong key detected!")
      return false
    end

  end

  private

  def set_status_waiting
    self.waiting!
  end


  def valid_iota_address
    if self.donation_address
      utils = IOTA::Utils::InputValidator.new
      errors.add(:donation_address, "Not a valid iota address.") unless utils.isAddress(self.donation_address)
    end
  end



end
