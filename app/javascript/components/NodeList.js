import React from "react"
import PropTypes from "prop-types"

import axios from "axios"


// Import React Table
import ReactTable from "react-table";

class NodeList extends React.Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      nodes: this.props.nodes
    };
  }

  render() {
    const columns = [{
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Points',
      accessor: 'points',
      accessor: 'points'
    }]

    return (

      <React.Fragment>
      <ReactTable
        data={this.state.nodes}
        columns={columns}
        defaultSorted={[
          {
            id: "points",
            desc: true
          }
        ]}
      />
      </React.Fragment>
    );
  }
}
export default NodeList
