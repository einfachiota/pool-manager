json.timestamp Time.now
json.entries_count @payout_data.count
json.entries do
  json.array! @payout_data, partial: 'nodes/payout_data', as: :payout_data
end
