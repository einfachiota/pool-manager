require 'rails_helper'

RSpec.describe Node, type: :model do
  before(:each) do
    @valid_attributes = {
      url: "https://node.einfachIOTA.de",
      name: "einfachIOTA Node",
      pow: false,
      donation_address: "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX"
    }
  end

  it "is valid with valid attributes" do
    node = Node.new(@valid_attributes)

    expect(node).to be_valid
  end

  it "is not valid without a url" do
    @valid_attributes[:url] = nil
    node = Node.new(@valid_attributes)

    expect(node).to_not be_valid
  end

  it "is not valid without a name" do
    @valid_attributes[:name] = nil
    node = Node.new(@valid_attributes)

    expect(node).to_not be_valid
  end


  it "is not valid without a invalid donation address" do
    @valid_attributes[:donation_address] = "ABCD1234"
    node = Node.new(@valid_attributes)

    expect(node).to_not be_valid
  end
end
