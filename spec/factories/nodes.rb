FactoryBot.define do
  factory :node do
    url { "MyString" }
    name { "MyString" }
    donation_address { "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX" }
    pow { false }
  end
end
