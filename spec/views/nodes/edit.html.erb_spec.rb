require 'rails_helper'

RSpec.describe "nodes/edit", type: :view do
  before(:each) do
    @node = assign(:node, Node.create!(
      :url => "MyString",
      :name => "MyString",
      :pow => false,
      :donation_address => "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX"
    ))
  end

  it "renders the edit node form" do
    render

    assert_select "form[action=?][method=?]", node_path(@node), "post" do

      assert_select "input[name=?]", "node[url]"

      assert_select "input[name=?]", "node[name]"

      assert_select "input[name=?]", "node[pow]"

      assert_select "input[donation_address=?]", "node[donation_address]"
    end
  end
end
