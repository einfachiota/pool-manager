require 'rails_helper'

RSpec.describe "nodes/index", type: :view do
  before(:each) do
    assign(:nodes, [
      Node.create!(
        :url => "Url",
        :name => "Name",
        :pow => false,
        :donation_address => "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX"
      ),
      Node.create!(
        :url => "Url",
        :name => "Name",
        :pow => false,
        :donation_address => "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUY"
      )
    ])
  end

  it "renders a list of nodes" do
    render
    assert_select "tr>td", :text => "Url".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
