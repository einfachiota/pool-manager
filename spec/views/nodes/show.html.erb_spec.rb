require 'rails_helper'

RSpec.describe "nodes/show", type: :view do
  before(:each) do
    @node = assign(:node, Node.create!(
      :url => "Url",
      :name => "Name",
      :pow => false,
      :donation_address => "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Url/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/false/)
  end
end
