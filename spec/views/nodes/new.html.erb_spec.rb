require 'rails_helper'

RSpec.describe "nodes/new", type: :view do
  before(:each) do
    assign(:node, Node.new(
      :url => "MyString",
      :name => "MyString",
      :pow => false
    ))
  end

  it "renders new node form" do
    render

    assert_select "form[action=?][method=?]", nodes_path, "post" do

      assert_select "input[name=?]", "node[url]"

      assert_select "input[name=?]", "node[name]"

      assert_select "input[name=?]", "node[pow]"
    end
  end
end
