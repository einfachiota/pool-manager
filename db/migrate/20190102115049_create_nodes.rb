class CreateNodes < ActiveRecord::Migration[5.2]
  def change
    create_table :nodes do |t|
      t.string :url
      t.string :name
      t.boolean :pow
      t.integer :status
      t.string :key

      t.timestamps
    end
  end
end
