class AddDonationAddressToNode < ActiveRecord::Migration[5.2]
  def change
    add_column :nodes, :donation_address, :string
  end
end
