# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

NODES = [
  {
    name: "einfachIOTA Node1",
    url: "1",
    pow: false,
    donation_address: "IORXRDCVFCTYQMQCO9KTZNNLQMGXOHWLVWNYRQIWKDTMZHWJ9DNYEITERDDEKPRIRMTBOCQHMNYCL9ZGX"
  },
  {
    name: "einfachIOTA Node2",
    url: "2",
    pow: false,
    donation_address: "Y9MZVQNLGBIQUIVAZXCHTBAQCDEEYMSUGDLXPACOUDHNUIGSYPLSMAWMKTUHQNFEIOMLYQD9GOZHKYFQA"
  },
  {
    name: "einfachIOTA Node3",
    url: "3",
    pow: false,
    donation_address: "EXUUFSUJQDJJSJIIDYORFLLKWYALGUADRHRBZQEZAHVDKRZOPJWOK9NQWKVYEWRJXMLE9DEMIWLRBJVKW"
  },
]


def create_nodes
  NODES.each do |node_params|
    node = Node.create(node_params)
    node.connected!
  end
end

create_nodes
